#!/usr/bin/env python3
#Copyright 2022 Fatih BATUM - BNTPRO 
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Script name derived from https://en.wikipedia.org/wiki/Grigori_Rasputin 
# be carefully while playing with it


import fileinput
import ipaddress
import sys
import argparse

def subneter(ip_dict, max_prefix):
    tmp_dict = {}
    
    for item in ip_dict:
        if ip_dict[item] < max_prefix:
            divide_to = 2 ** (max_prefix - ip_dict[item])
            net = ipaddress.IPv4Address(item)
            for k in range(0,divide_to):
                tmp_dict[ipaddress.IPv4Address(net + k * (2** (32 - max_prefix)))]=max_prefix
        else:
                tmp_dict[item]=int(ip_dict[item])

    return tmp_dict

def supernetting(ip_dict):
    net_list=[]
    tmp_dict = sorted(ip_dict.items(), key=lambda x: x[1], reverse=True)
    for item in tmp_dict:
         net_list.append(item[0]+'/'+str(item[1]))
    ip_dict=(ipaddress.collapse_addresses([ipaddress.IPv4Network(i) for i in net_list]))
    tmp_dict={}
    for item in ip_dict:
         _p=str(item).split('/')
         tmp_dict[_p[0]]=int(_p[1])
 
    return tmp_dict

def balance(ip_dict, srv_count, reverse):
    t_dict={}
    t_dict_order={}
    total_host=0
    for item in ip_dict:
        total_host+=2**(32-int(ip_dict[item]))
    balanced_host=total_host /srv_count
#    masks=(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32)
    masks=range(1,32,1)
    for m in masks:
        if 2**m >= balanced_host:
            balanced_pref = m 
            break 
    srv_list=[*range(1, srv_count+1, 1)]
    u=0
    sort_orders = sorted(ip_dict.items(), key=lambda x: x[1], reverse=reverse)


    for n in range(0,len(ip_dict.keys())):
        for j in range(1,srv_count+1,1):
            if u <= len(ip_dict.keys()) - 1:
                t_dict[sort_orders[u][0]]=sort_orders[u][1]
                t_dict_order[sort_orders[u][0]]=j
                u+=1

        for j in range(srv_count,0,-1):
            if u <= len(ip_dict.keys()) - 1:
                t_dict[sort_orders[u][0]]=sort_orders[u][1]
                t_dict_order[sort_orders[u][0]]=j
                u+=1
    return t_dict, t_dict_order 


def main(argv=None):

    global networks
    ip_dict={}
    host_count=0
    reverse = False
    csv_format=False
    argv = sys.argv
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--max-prefix", type=int, help='Deciding to maximum prefix lenghth for subnets (31-1)', required=False)
    parser.add_argument("-g", "--group-number", type=int, help='Number of destination groups that the prefixes will be load balanced',required=False, default=1)
    parser.add_argument("-a", "--ascending", action='store_true', help='Sorts the list ascending, default behavior is disascending',required=False)
    parser.add_argument("-c", "--collapse", action='store_true', help='Supernets the prefixes provided', required=False)
    parser.add_argument("-f", "--csv-formatted", action='store_true',help='Format output as comma seperated value', required=False)   
    args = parser.parse_args()
    argv_cnt=1

    if args.max_prefix:
        argv_cnt+=2
    if args.group_number > 1:
        argv_cnt+=2
    if args.ascending:
        argv_cnt+=1
        reverse = True
    if args.collapse:
        argv_cnt+=1
    if args.csv_formatted:
        csv_format=True
        argv_cnt+=1
   
    for line in fileinput.input([x for x in argv[argv_cnt:] if not x.startswith('-')]):
        line_pref=line.split('/')
        host_count += 2**(32-int(line_pref[1]))
        if (line_pref[0] in ip_dict) and int(line_pref[1]) > ip_dict[line_pref[0]]:
            continue
        else:
            ip_dict[line_pref[0]]=int(line_pref[1]) 

    if args.collapse:
        ip_dict=supernetting(ip_dict)

    if args.max_prefix:
        ip_dict = subneter(ip_dict,args.max_prefix)

    ip_dict, dict_order = balance(ip_dict,args.group_number, reverse)

    for item in ip_dict:
        if csv_format==True:
            print ("%s;%d;%d" % (item, ip_dict[item], dict_order[item]))
        else:
            print ("%s/%d:%d" % (item, ip_dict[item], dict_order[item]))

if __name__ == "__main__":
    sys.exit(main())